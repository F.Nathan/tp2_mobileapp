package com.example.uapv1700716.tp2;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class BookActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getName();

    BookDbHelper db = new BookDbHelper(this);
    Book book = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        Intent intent = getIntent();
        book = intent.getParcelableExtra("book");

        TextView name = (TextView) findViewById(R.id.nameBook);
        TextView editAuthors = (TextView) findViewById(R.id.editAuthors);
        TextView editYear = (TextView) findViewById(R.id.editYear);
        TextView editGenres = (TextView) findViewById(R.id.editGenres);
        TextView editPublisher = (TextView) findViewById(R.id.editPublisher);

        if (book != null) {
            name.setText(book.getTitle());
            editAuthors.setText(book.getAuthors());
            editYear.setText(book.getYear());
            editGenres.setText(book.getGenres());
            editPublisher.setText(book.getPublisher());
        }


        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean newBook = false;

                TextView name = (TextView) findViewById(R.id.nameBook);
                TextView editAuthors = (TextView) findViewById(R.id.editAuthors);
                TextView editYear = (TextView) findViewById(R.id.editYear);
                TextView editGenres = (TextView) findViewById(R.id.editGenres);
                TextView editPublisher = (TextView) findViewById(R.id.editPublisher);

                if (book == null) { book = new Book(); newBook = true;}

                book.setTitle(name.getText().toString());
                book.setAuthors(editAuthors.getText().toString());
                book.setYear(editYear.getText().toString());
                book.setGenres(editGenres.getText().toString());
                book.setPublisher(editPublisher.getText().toString());

                if (newBook == true) db.addBook(book);
                else db.updateBook(book);

                finish();
            }
        });

    }
}
