package com.example.uapv1700716.tp2;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.database.Cursor;
import com.example.uapv1700716.tp2.BookDbHelper;
import com.example.uapv1700716.tp2.Book;

import android.support.v4.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getName();

    BookDbHelper db = new BookDbHelper(this);
    Book book = null;
    ListView bookListView = null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Log.d(TAG, "onCreate: ");(db.fetchAllBooks().getCount());
        if (db.fetchAllBooks().getCount() < 1) {
            db.populate();
        }

        final SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                db.fetchAllBooks(),
                new String[] { "title","authors" },
                new int[] { android.R.id.text1, android.R.id.text2 });

        bookListView = (ListView) findViewById(R.id.listView_1);
        bookListView.setAdapter(adapter);

        registerForContextMenu(bookListView);

        bookListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3)
            {
                final Cursor selectedBook = (Cursor)adapter.getItemAtPosition(position);

                book = db.cursorToBook(selectedBook);

                Intent intent = new Intent(MainActivity.this, BookActivity.class);
                intent.putExtra("book", book);
                startActivity(intent);

            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Book book = null;
                Intent intent = new Intent(MainActivity.this, BookActivity.class);
                intent.putExtra("book", book);
                startActivity(intent);

                /*
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                */
            }
        });




    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*
        if (id == R.id.action_settings) {
            return true;
        }
        */
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        //menu.setHeaderTitle("Select The Action");
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        AdapterView v  = findViewById(R.id.listView_1);
        Cursor selectedBook = (Cursor) v.getItemAtPosition(menuInfo.position);
        db.deleteBook(selectedBook);
        this.onResume();

        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (db.fetchAllBooks().getCount() < 1) {
            db.populate();
        }

        final SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                db.fetchAllBooks(),
                new String[] { "title","authors" },
                new int[] { android.R.id.text1, android.R.id.text2 });

        bookListView = (ListView) findViewById(R.id.listView_1);
        bookListView.setAdapter(adapter);
    }
}

